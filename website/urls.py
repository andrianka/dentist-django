from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('contact', views.contact, name="contact"),
    path('price', views.price, name="price"),
    path('about', views.about, name="about"),
]
